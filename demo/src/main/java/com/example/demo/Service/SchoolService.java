package com.example.demo.Service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.example.demo.Model.School;
@Service
public class SchoolService {
    
    public static ArrayList<School> getSchoolList() {
        ArrayList<School> schoolList = new ArrayList<School>();

        School TieuHoc = new School(1,"Tiểu Học", "Khánh Hội Q1" , ClassroomService.getClassTieuHoc());
        School TrungHoc = new School(2,"Trung Học", "Nguyễn Khuyến Q10" , ClassroomService.getClassTrungHoc());
        School PhoThong = new School(3,"Phổ Thông", "Nguyễn Hữu Thọ Q4" , ClassroomService.getClassPhoThong());

        schoolList.add(TieuHoc);
        schoolList.add(TrungHoc);
        schoolList.add(PhoThong);

        return schoolList;
    }

    
}
