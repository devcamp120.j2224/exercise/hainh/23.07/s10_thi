package com.example.demo.Service;

import java.util.ArrayList;
import org.springframework.stereotype.Service;

import com.example.demo.Model.Classroom;

@Service
public class ClassroomService {

    public static ArrayList<Classroom> getClassTieuHoc(){
        ArrayList<Classroom> ClassVN = new ArrayList<Classroom>();

        Classroom lop1 = new Classroom(1,"Lớp 1",30 );
        Classroom lop2 = new Classroom(1, "Lớp 2",20);
        Classroom lop3 = new Classroom(1, "Lớp 3",60);

        ClassVN.add(lop1);
        ClassVN.add(lop2);
        ClassVN.add(lop3);
       
        return ClassVN;
    }

   
    public static ArrayList<Classroom> getClassTrungHoc(){
        ArrayList<Classroom> ClassJP = new ArrayList<Classroom>();

        Classroom lop3 = new Classroom(2, "Lớp 4",10);
        Classroom lop4 = new Classroom(2, "Lớp 5",40);
        Classroom lop5 = new Classroom(2, "Lớp 6",50);

        ClassJP.add(lop3);
        ClassJP.add(lop4);
        ClassJP.add(lop5);

        return ClassJP;
    }
    public static ArrayList<Classroom> getClassPhoThong(){
        ArrayList<Classroom> ClassUS = new ArrayList<Classroom>();

        Classroom lop6 = new Classroom(3, "Lớp 7",70);
        Classroom lop7 = new Classroom(3, "Lớp 8",80);
        Classroom lop8 = new Classroom(3, "Lớp 9",90);

        ClassUS.add(lop6);
        ClassUS.add(lop7);
        ClassUS.add(lop8);

        return ClassUS;
    }

    public static ArrayList<Classroom> getClassAll() {
        ArrayList<Classroom> classAll = new ArrayList<Classroom>();

        classAll.addAll(getClassTieuHoc());
        classAll.addAll(getClassTrungHoc());
        classAll.addAll(getClassPhoThong());

        return classAll;
        
    }
}
