package com.example.demo.Controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.School;
import com.example.demo.Service.SchoolService;

@RestController
public class SchoolController {
    

    @GetMapping("/school-info")
    public School getSchoolInfo(@RequestParam("schoolId") int schoolId){
        ArrayList<School> schools = SchoolService.getSchoolList() ;

        School findSchool = new School();
        for(School school : schools){
            if(school.getId() == schoolId){
                findSchool = school ;
            }
        }
        return findSchool ;
    }

    @GetMapping("/schoolsAll")
    public ArrayList<School> getSchoolAll(){
        ArrayList<School> schools = SchoolService.getSchoolList();
        return schools;
    }

    @GetMapping("/school-noNumber")
    public ArrayList<School> getSchoolByStudentApi(@RequestParam("noNumber") int noNumber){
        ArrayList<School> schools = SchoolService.getSchoolList();
        ArrayList<School> schoolsfind = new ArrayList<>();
        // resul sẽ bằng class School.getTotalStudent đã khai báo ở class School
        // phải để bằng mới đúng , += là sai 
        for(School i : schools){
            int resul = 0 ;
            resul = i.getTotalStudent();
            if(resul > noNumber){
                schoolsfind.add(i);
            }
        }
        return schoolsfind ;
    }
}

